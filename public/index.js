let txtAuthor = document.getElementById("txtAuthor");
let txtMessage = document.getElementById("txtMessage");
let divTopicList = document.getElementById("divTopicList");
let file = document.getElementById("file");

// prepareRender();

// function prepareRender() {
//     firebase.database().ref('/Guestbook').once('value').then(function(snapshot) {
//         let tempTopicList = '';
//         for (let key in snapshot.val()) {
//             tempTopicList = tempTopicList + `<div style="border: 1px #adadad solid;">
//                                                 <div style="background-color: #585b5f; height: 2%;"></div>
//                                                 <p style="padding-left: 1%;"><strong>${snapshot.val()[key].name}</strong> at ${snapshot.val()[key].date}</p>
//                                                 <p style="padding-left: 3%; font-style: italic;">${snapshot.val()[key].message}</p>
                                                
//                                                 <button id="btnEditItem" type="button" name="btnEdit" class="myBtnEdit" data-toggle="modal" data-target="#myModal"
//                                                                     data-id="${key}" onclick="editFunction('${key}')">
//                                                                     <i class="fa fa fa-pencil"></i>
//                                                                 </button>

//                                                 <p><button id="delBtn" class="btnDel" onclick="deleteBtn('${key}')"><i class="fa fa fa-close" aria-hidden="true"></i></button></p>
//                                             </div><br>`;
//         }
//         divTopicList.innerHTML=tempTopicList;
//     });
// }

let starCountRef = firebase.database().ref('/Guestbook/');
starCountRef.on('value', function(snapshot) {
    let tempTopicList = '';
    for (let key in snapshot.val()) {
        tempTopicList = tempTopicList + `<div style="border: 1px #adadad solid; background-color: #eaeaea; color: black; border-bottom-left-radius: 7px; border-bottom-right-radius: 7px;">

                                            <div class="avatar">
                                                <img src="${snapshot.val()[key].avatar}" alt="Avatar" width="300" height="200">
                                            </div>

                                            <div style="background-color: #585b5f; height: 2%;"></div>
                                            <p style="padding-left: 1%;"><strong>${snapshot.val()[key].name}</strong> at ${snapshot.val()[key].date}</p>
                                            <p style="padding-left: 3%; font-style: italic;">${snapshot.val()[key].message}</p>
                                            
                                            <button id="btnEditItem" type="button" name="btnEdit" class="myBtnEdit" data-toggle="modal" data-target="#myModal"
                                                data-id="${key}" onclick="editFunction('${key}')">
                                                <i class="fa fa fa-pencil"></i>
                                            </button>

                                            <p><button id="delBtn" class="btnDel" onclick="deleteBtn('${key}')"><i class="fa fa fa-close" aria-hidden="true"></i></button></p>
                                        </div><br>`;
    }
    divTopicList.innerHTML=tempTopicList;
});

function postSubmit() {

    if(file.files[0]) {
        //Upload Picture
        var storageRef = firebase.storage().ref();
        var uploadTask = storageRef.child('avatarImages/' + file.files[0].name).put(file.files[0]);

        uploadTask.on('state_changed', function(snapshot){

        }, function(error) {
            console.error("error >>> ", error);
        }, function() {
            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                // A post entry.
                let currentdate = new Date(); 
                let datetime = currentdate.getDate() + "/" + (currentdate.getMonth()+1)  + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

                var postData = {
                    name: txtAuthor.value,
                    message: txtMessage.value,
                    avatar: downloadURL,
                    date : datetime
                };
            
                // Get a key for a new Post.
                var newPostKey = firebase.database().ref().child('Guestbook').push().key;
                
                // Write the new post's data simultaneously in the posts list and the user's post list.
                var updates = {};
                updates['Guestbook/list_' + newPostKey] = postData;

                txtAuthor.value = "";
                txtMessage.value = "";
                file.value = "";
            
                return firebase.database().ref().update(updates);
            });
        });
    }
    else {
        // A post entry.
        let currentdate = new Date(); 
        let datetime = currentdate.getDate() + "/" + (currentdate.getMonth()+1)  + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

        var postData = {
            name: txtAuthor.value,
            message: txtMessage.value,
            avatar: "https://firebasestorage.googleapis.com/v0/b/natthakitfirebaseweb.appspot.com/o/avatarImages%2FavatarDefault.png?alt=media&token=7178d7c5-7aca-4b9b-aa29-62029f47d800",
            date : datetime
        };
    
        // Get a key for a new Post.
        var newPostKey = firebase.database().ref().child('Guestbook').push().key;
        
        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        updates['Guestbook/list_' + newPostKey] = postData;

        txtAuthor.value = "";
        txtMessage.value = "";
        file.value = "";
    
        return firebase.database().ref().update(updates);
    }
    
}

function deleteBtn(item){
    let x = confirm("Are you sure you want to delete?");
    if(x) {
        firebase.database().ref('/Guestbook/'+ item).remove();
        return true;
    }
    else {
        return false;
    }
}

//Edit Button In Modal
function editFunction(arg) {
    firebase.database().ref('/Guestbook/' + arg).once('value').then(function(snapshot) {
        document.getElementById("modalText").innerText = snapshot.val().name;
        document.getElementById("txtMessageModal").value = snapshot.val().message;
        document.getElementById("listId").value = arg;
    });
}

function editfunc() {
    firebase.database().ref('/Guestbook/' + document.getElementById("listId").value).update(
        { message : document.getElementById("txtMessageModal").value }
    );
}

// let uploadButton = document.getElementById("uploadButton");
// uploadButton.style.visibility = "hidden";

// function uploadChange() {
//     uploadButton.style.visibility = "visible";
// }

// function uploadFile() {
//     var storageRef = firebase.storage().ref();
//     var uploadTask = storageRef.child('avatarImages/' + file.files[0].name).put(file.files[0]);

//     uploadTask.on('state_changed', function(snapshot){

//     }, function(error) {
//         console.error("error >>> ", error);
//     }, function() {
//         uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
//             console.log('File available at', downloadURL);
//         });
//     });
// }